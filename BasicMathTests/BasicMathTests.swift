//
//  BasicMathTests.swift
//  BasicMathTests
//
//  Created by CCITESTING on 28/09/15.
//  Copyright © 2015 CreativeCapsule. All rights reserved.
//

import XCTest
import BasicMath

class BasicMathTests: XCTestCase {
    let basicMath = BasicMath()
    var firstNum = 34.0
    var secondNum = 23.0
    
    let firstNumDiv: Float = 24.0
    let secNumDiv:Float = 45.0

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testAddTwoNumbers()
    {
        let result = basicMath.addTwoNumbers(firstNum, second: secondNum)
        
        XCTAssertEqual(result, firstNum + secondNum)
    }
    
    func testSubtractTwoNumbers()
    {
        let result = basicMath.subtractTwoNumbers(firstNum, second: secondNum)
        XCTAssertEqual(result, firstNum - secondNum)
    }
    
    func testMultiplyTwoNumbers(){
        let result = basicMath.multiplyTwoNumbers(firstNum, second: secondNum)
        XCTAssertEqual(result, firstNum * secondNum)
    }
    
    func testDivideTwoNumbers(){
        let result = basicMath.divideTwoNumbers(firstNumDiv, second: secNumDiv)
        XCTAssertEqual(result, firstNumDiv / secNumDiv)
        
    }
}
