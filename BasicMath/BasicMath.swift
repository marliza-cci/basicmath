//
//  BasicMath.swift
//  BasicMath
//
//  Created by CCITESTING on 28/09/15.
//  Copyright © 2015 CreativeCapsule. All rights reserved.
//

import UIKit

public class BasicMath: NSObject {
    
    // Addition of two numbers
    public func addTwoNumbers(first: Double, second: Double) -> Double{
        
        return first + second;
    }
    
    public func subtractTwoNumbers(first: Double, second: Double) -> Double{
        
        return first - second;
    }
    
    public func multiplyTwoNumbers(first: Double, second: Double) -> Double{
        return first * second
    }
    
    public func divideTwoNumbers(first: Double, second:Double) -> Double{
        if (second != 0 ){
            return  first / second
        }else
        {
            return -1 //"Division by zero is not allowed"
            
        }
    }
    
}
