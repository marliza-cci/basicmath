//
//  ViewController.swift
//  BasicMath
//
//  Created by CCITESTING on 28/09/15.
//  Copyright © 2015 CreativeCapsule. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let basicMath = BasicMath()
        let result =  basicMath.divideTwoNumbers(24, second: 0)
        print(result)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   }

